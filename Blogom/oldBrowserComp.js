"use strict";

React.createElement(
    "html",
    { lang: "en" },
    React.createElement(
        "head",
        null,
        React.createElement("meta", { charset: "UTF-8" }),
        React.createElement("meta", { name: "viewport", content: "width=device-width, initial-scale=1.0" }),
        React.createElement("meta", { "http-equiv": "X-UA-Compatible", content: "ie=edge" }),
        React.createElement(
            "title",
            null,
            "MMMco"
        ),
        React.createElement("link", { rel: "stylesheet", href: "blog.css" }),
        React.createElement("link", { rel: "stylesheet", href: "css/font-awesome.css" }),
        React.createElement("link", { rel: "stylesheet/less", type: "text/css", href: "blog.less" })
    ),
    React.createElement(
        "body",
        null,
        React.createElement(
            "header",
            null,
            React.createElement(
                "div",
                { id: "headerdiv" },
                React.createElement(
                    "h1",
                    { id: "headertext" },
                    "Magpie Music Mining Co."
                )
            ),
            React.createElement(
                "nav",
                null,
                React.createElement(
                    "div",
                    { "class": "divnav" },
                    React.createElement(
                        "a",
                        { "class": "nava", href: "./Blog.html", title: "Home" },
                        React.createElement(
                            "div",
                            { "class": "navdiv" },
                            "Home"
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { "class": "divnav" },
                    React.createElement(
                        "a",
                        { "class": "nava", href: "#", title: "Topics" },
                        React.createElement(
                            "div",
                            { "class": "navdiv" },
                            "Topics"
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { "class": "divnav" },
                    React.createElement(
                        "a",
                        { "class": "nava", href: "./blogabout.html", title: "About" },
                        React.createElement(
                            "div",
                            { "class": "navdiv" },
                            "About"
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { "class": "divnav" },
                    React.createElement(
                        "a",
                        { "class": "nava", href: "./blogcontact.html", title: "Contact" },
                        React.createElement(
                            "div",
                            { "class": "navdiv" },
                            "Contact"
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { "class": "divnav" },
                    React.createElement(
                        "a",
                        { "class": "nava", href: "./blogsearch.html", title: "Search" },
                        React.createElement(
                            "div",
                            { "class": "navdiv" },
                            "Search"
                        )
                    )
                )
            )
        ),
        React.createElement(
            "main",
            { id: "lofasz" },
            React.createElement(
                "div",
                { "class": "post" },
                React.createElement(
                    "h2",
                    { id: "blogcim" },
                    React.createElement(
                        "a",
                        { href: "./POST01.html", title: "POST01", "class": "postlink" },
                        "POST01"
                    )
                ),
                React.createElement("div", { "class": "fade" }),
                React.createElement(
                    "p",
                    { "class": "posting" },
                    React.createElement("img", { src: "https://picsum.photos/g/300/300", alt: "pikcs\xF6\xF6\xF6\xF6r", "class": "postpicture" }),
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac orci elementum, commodo arcu ac, commodo metus. Aenean facilisis suscipit orci, vitae egestas enim ultricies suscipit. Quisque lacinia egestas dolor. Quisque vitae sollicitudin ipsum, sit amet aliquet libero. Ut sed finibus ipsum. Nam vestibulum id lacus et pharetra. Duis quis enim quam. In hac habitasse platea dictumst. Sed congue lectus quis erat sollicitudin lacinia. Aenean porttitor fermentum porttitor. Morbi sit amet metus volutpat, vehicula libero vel, cursus tortor. Mauris elementum risus urna, at semper diam bibendum vel. Cras volutpat tellus a magna rhoncus dictum. Nunc maximus turpis dictum dui commodo tempor. Nunc suscipit, purus in egestas tincidunt, est lorem bibendum ex, at pulvinar nunc orci nec lorem. Nullam nec massa iaculis, ullamcorper metus at, tempor augue. Integer accumsan blandit nisi et venenatis. Phasellus ac pharetra elit. Vestibulum quis nulla eget dolor congue tempus. Aliquam erat volutpat. Donec ultricies sem et nisl aliquet cursus. Sed sed risus tincidunt, lobortis magna in, blandit enim. Mauris mattis laoreet elit in accumsan. Vivamus efficitur nisi at ipsum facilisis convallis. Vestibulum quis tincidunt dui, eu feugiat sem. Fusce imperdiet massa metus, sit amet hendrerit diam tincidunt eu. Aliquam erat volutpat. Sed sit amet fermentum sapien, vel porttitor sem. Nunc ullamcorper convallis felis, a dictum ante consectetur eget. Ut sit amet pellentesque ante. Nulla at nisi imperdiet, ullamcorper dolor nec, posuere est. Curabitur semper ultricies scelerisque. Suspendisse auctor in diam quis iaculis. Integer mollis velit at turpis porttitor, sed gravida tortor tempus. Donec placerat lectus a turpis auctor faucibus in vitae odio. Sed enim sapien, blandit et sem eu, suscipit molestie velit. Nunc ultrices tortor vel elit porta, eu tincidunt lacus laoreet. Morbi gravida sed quam blandit ultrices. Pellentesque interdum lorem sit amet mattis commodo. Etiam et lectus euismod, mollis mi et, mattis lacus. Ut eget pulvinar magna, ornare ultrices mi. Curabitur hendrerit neque non sem sagittis ultricies. Suspendisse massa erat, hendrerit at turpis cursus, iaculis maximus massa. Mauris vel porta mi, quis ornare sem. Nam vel quam turpis. Nullam viverra ex augue, luctus dictum nunc tincidunt vel. Morbi facilisis elit a tempor scelerisque. Donec elementum posuere dui nec congue. Fusce viverra eros auctor facilisis facilisis. Nulla facilisi. Praesent sem justo, sodales mollis cursus quis, sollicitudin ut metus. Mauris risus nulla, auctor laoreet hendrerit nec, dignissim ut sem."
                ),
                React.createElement(
                    "div",
                    { "class": "afterpost" },
                    React.createElement(
                        "h4",
                        null,
                        "Author"
                    ),
                    React.createElement(
                        "h4",
                        null,
                        React.createElement(
                            "a",
                            { href: "./POST01.html", title: "POST02", "class": "postlink" },
                            "Show more"
                        )
                    ),
                    React.createElement(
                        "h4",
                        null,
                        "Comments"
                    )
                )
            ),
            React.createElement(
                "div",
                { "class": "post" },
                React.createElement(
                    "h2",
                    { id: "blogcim" },
                    React.createElement(
                        "a",
                        { href: "#", title: "POST02", "class": "postlink" },
                        "POST02"
                    )
                ),
                React.createElement("div", { "class": "fade" }),
                React.createElement(
                    "p",
                    { "class": "posting" },
                    React.createElement("img", { src: "https://picsum.photos/g/300/300", alt: "pikcs\xF6\xF6\xF6\xF6r", "class": "postpicture" }),
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac orci elementum, commodo arcu ac, commodo metus. Aenean facilisis suscipit orci, vitae egestas enim ultricies suscipit. Quisque lacinia egestas dolor. Quisque vitae sollicitudin ipsum, sit amet aliquet libero. Ut sed finibus ipsum. Nam vestibulum id lacus et pharetra. Duis quis enim quam. In hac habitasse platea dictumst. Sed congue lectus quis erat sollicitudin lacinia. Aenean porttitor fermentum porttitor. Morbi sit amet metus volutpat, vehicula libero vel, cursus tortor. Mauris elementum risus urna, at semper diam bibendum vel. Cras volutpat tellus a magna rhoncus dictum. Nunc maximus turpis dictum dui commodo tempor. Nunc suscipit, purus in egestas tincidunt, est lorem bibendum ex, at pulvinar nunc orci nec lorem. Nullam nec massa iaculis, ullamcorper metus at, tempor augue. Integer accumsan blandit nisi et venenatis. Phasellus ac pharetra elit. Vestibulum quis nulla eget dolor congue tempus. Aliquam erat volutpat. Donec ultricies sem et nisl aliquet cursus. Sed sed risus tincidunt, lobortis magna in, blandit enim. Mauris mattis laoreet elit in accumsan. Vivamus efficitur nisi at ipsum facilisis convallis. Vestibulum quis tincidunt dui, eu feugiat sem. Fusce imperdiet massa metus, sit amet hendrerit diam tincidunt eu. Aliquam erat volutpat. Sed sit amet fermentum sapien, vel porttitor sem. Nunc ullamcorper convallis felis, a dictum ante consectetur eget. Ut sit amet pellentesque ante. Nulla at nisi imperdiet, ullamcorper dolor nec, posuere est. Curabitur semper ultricies scelerisque. Suspendisse auctor in diam quis iaculis. Integer mollis velit at turpis porttitor, sed gravida tortor tempus. Donec placerat lectus a turpis auctor faucibus in vitae odio. Sed enim sapien, blandit et sem eu, suscipit molestie velit. Nunc ultrices tortor vel elit porta, eu tincidunt lacus laoreet. Morbi gravida sed quam blandit ultrices. Pellentesque interdum lorem sit amet mattis commodo. Etiam et lectus euismod, mollis mi et, mattis lacus. Ut eget pulvinar magna, ornare ultrices mi. Curabitur hendrerit neque non sem sagittis ultricies. Suspendisse massa erat, hendrerit at turpis cursus, iaculis maximus massa. Mauris vel porta mi, quis ornare sem. Nam vel quam turpis. Nullam viverra ex augue, luctus dictum nunc tincidunt vel. Morbi facilisis elit a tempor scelerisque. Donec elementum posuere dui nec congue. Fusce viverra eros auctor facilisis facilisis. Nulla facilisi. Praesent sem justo, sodales mollis cursus quis, sollicitudin ut metus. Mauris risus nulla, auctor laoreet hendrerit nec, dignissim ut sem."
                ),
                React.createElement(
                    "div",
                    { "class": "afterpost" },
                    React.createElement(
                        "h4",
                        null,
                        "Author"
                    ),
                    React.createElement(
                        "h4",
                        null,
                        React.createElement(
                            "a",
                            { href: "#", title: "POST02", "class": "postlink" },
                            "Show more"
                        )
                    ),
                    React.createElement(
                        "h4",
                        null,
                        "Comments"
                    )
                )
            ),
            React.createElement(
                "div",
                { "class": "post" },
                React.createElement(
                    "h2",
                    { id: "blogcim" },
                    React.createElement(
                        "a",
                        { href: "#", title: "POST03", "class": "postlink" },
                        "POST03"
                    )
                ),
                React.createElement("div", { "class": "fade" }),
                React.createElement(
                    "p",
                    { "class": "posting" },
                    React.createElement("img", { src: "https://picsum.photos/g/300/300", alt: "pikcs\xF6\xF6\xF6\xF6r", "class": "postpicture" }),
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac orci elementum, commodo arcu ac, commodo metus. Aenean facilisis suscipit orci, vitae egestas enim ultricies suscipit. Quisque lacinia egestas dolor. Quisque vitae sollicitudin ipsum, sit amet aliquet libero. Ut sed finibus ipsum. Nam vestibulum id lacus et pharetra. Duis quis enim quam. In hac habitasse platea dictumst. Sed congue lectus quis erat sollicitudin lacinia. Aenean porttitor fermentum porttitor. Morbi sit amet metus volutpat, vehicula libero vel, cursus tortor. Mauris elementum risus urna, at semper diam bibendum vel. Cras volutpat tellus a magna rhoncus dictum. Nunc maximus turpis dictum dui commodo tempor. Nunc suscipit, purus in egestas tincidunt, est lorem bibendum ex, at pulvinar nunc orci nec lorem. Nullam nec massa iaculis, ullamcorper metus at, tempor augue. Integer accumsan blandit nisi et venenatis. Phasellus ac pharetra elit. Vestibulum quis nulla eget dolor congue tempus. Aliquam erat volutpat. Donec ultricies sem et nisl aliquet cursus. Sed sed risus tincidunt, lobortis magna in, blandit enim. Mauris mattis laoreet elit in accumsan. Vivamus efficitur nisi at ipsum facilisis convallis. Vestibulum quis tincidunt dui, eu feugiat sem. Fusce imperdiet massa metus, sit amet hendrerit diam tincidunt eu. Aliquam erat volutpat. Sed sit amet fermentum sapien, vel porttitor sem. Nunc ullamcorper convallis felis, a dictum ante consectetur eget. Ut sit amet pellentesque ante. Nulla at nisi imperdiet, ullamcorper dolor nec, posuere est. Curabitur semper ultricies scelerisque. Suspendisse auctor in diam quis iaculis. Integer mollis velit at turpis porttitor, sed gravida tortor tempus. Donec placerat lectus a turpis auctor faucibus in vitae odio. Sed enim sapien, blandit et sem eu, suscipit molestie velit. Nunc ultrices tortor vel elit porta, eu tincidunt lacus laoreet. Morbi gravida sed quam blandit ultrices. Pellentesque interdum lorem sit amet mattis commodo. Etiam et lectus euismod, mollis mi et, mattis lacus. Ut eget pulvinar magna, ornare ultrices mi. Curabitur hendrerit neque non sem sagittis ultricies. Suspendisse massa erat, hendrerit at turpis cursus, iaculis maximus massa. Mauris vel porta mi, quis ornare sem. Nam vel quam turpis. Nullam viverra ex augue, luctus dictum nunc tincidunt vel. Morbi facilisis elit a tempor scelerisque. Donec elementum posuere dui nec congue. Fusce viverra eros auctor facilisis facilisis. Nulla facilisi. Praesent sem justo, sodales mollis cursus quis, sollicitudin ut metus. Mauris risus nulla, auctor laoreet hendrerit nec, dignissim ut sem."
                ),
                React.createElement(
                    "div",
                    { "class": "afterpost" },
                    React.createElement(
                        "h4",
                        null,
                        "Author"
                    ),
                    React.createElement(
                        "h4",
                        null,
                        React.createElement(
                            "a",
                            { href: "#", title: "POST02", "class": "postlink" },
                            "Show more"
                        )
                    ),
                    React.createElement(
                        "h4",
                        null,
                        "Comments"
                    )
                )
            ),
            React.createElement(
                "div",
                { "class": "post" },
                React.createElement(
                    "h2",
                    { id: "blogcim" },
                    React.createElement(
                        "a",
                        { href: "#", title: "POST04", "class": "postlink" },
                        "POST04"
                    )
                ),
                React.createElement("div", { "class": "fade" }),
                React.createElement(
                    "p",
                    { "class": "posting" },
                    React.createElement("img", { src: "https://picsum.photos/g/300/300", alt: "pikcs\xF6\xF6\xF6\xF6r", "class": "postpicture" }),
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac orci elementum, commodo arcu ac, commodo metus. Aenean facilisis suscipit orci, vitae egestas enim ultricies suscipit. Quisque lacinia egestas dolor. Quisque vitae sollicitudin ipsum, sit amet aliquet libero. Ut sed finibus ipsum. Nam vestibulum id lacus et pharetra. Duis quis enim quam. In hac habitasse platea dictumst. Sed congue lectus quis erat sollicitudin lacinia. Aenean porttitor fermentum porttitor. Morbi sit amet metus volutpat, vehicula libero vel, cursus tortor. Mauris elementum risus urna, at semper diam bibendum vel. Cras volutpat tellus a magna rhoncus dictum. Nunc maximus turpis dictum dui commodo tempor. Nunc suscipit, purus in egestas tincidunt, est lorem bibendum ex, at pulvinar nunc orci nec lorem. Nullam nec massa iaculis, ullamcorper metus at, tempor augue. Integer accumsan blandit nisi et venenatis. Phasellus ac pharetra elit. Vestibulum quis nulla eget dolor congue tempus. Aliquam erat volutpat. Donec ultricies sem et nisl aliquet cursus. Sed sed risus tincidunt, lobortis magna in, blandit enim. Mauris mattis laoreet elit in accumsan. Vivamus efficitur nisi at ipsum facilisis convallis. Vestibulum quis tincidunt dui, eu feugiat sem. Fusce imperdiet massa metus, sit amet hendrerit diam tincidunt eu. Aliquam erat volutpat. Sed sit amet fermentum sapien, vel porttitor sem. Nunc ullamcorper convallis felis, a dictum ante consectetur eget. Ut sit amet pellentesque ante. Nulla at nisi imperdiet, ullamcorper dolor nec, posuere est. Curabitur semper ultricies scelerisque. Suspendisse auctor in diam quis iaculis. Integer mollis velit at turpis porttitor, sed gravida tortor tempus. Donec placerat lectus a turpis auctor faucibus in vitae odio. Sed enim sapien, blandit et sem eu, suscipit molestie velit. Nunc ultrices tortor vel elit porta, eu tincidunt lacus laoreet. Morbi gravida sed quam blandit ultrices. Pellentesque interdum lorem sit amet mattis commodo. Etiam et lectus euismod, mollis mi et, mattis lacus. Ut eget pulvinar magna, ornare ultrices mi. Curabitur hendrerit neque non sem sagittis ultricies. Suspendisse massa erat, hendrerit at turpis cursus, iaculis maximus massa. Mauris vel porta mi, quis ornare sem. Nam vel quam turpis. Nullam viverra ex augue, luctus dictum nunc tincidunt vel. Morbi facilisis elit a tempor scelerisque. Donec elementum posuere dui nec congue. Fusce viverra eros auctor facilisis facilisis. Nulla facilisi. Praesent sem justo, sodales mollis cursus quis, sollicitudin ut metus. Mauris risus nulla, auctor laoreet hendrerit nec, dignissim ut sem."
                ),
                React.createElement(
                    "div",
                    { "class": "afterpost" },
                    React.createElement(
                        "h4",
                        null,
                        "Author"
                    ),
                    React.createElement(
                        "h4",
                        null,
                        React.createElement(
                            "a",
                            { href: "#", title: "POST02", "class": "postlink" },
                            "Show more"
                        )
                    ),
                    React.createElement(
                        "h4",
                        null,
                        "Comments"
                    )
                )
            )
        ),
        React.createElement(
            "aside",
            null,
            React.createElement(
                "div",
                { "class": "massofsocial" },
                React.createElement(
                    "h2",
                    null,
                    "Sell your soul into the company store:"
                ),
                React.createElement(
                    "div",
                    { "class": "social" },
                    React.createElement(
                        "a",
                        { "class": "facebooklink", href: "https://www.facebook.com" },
                        React.createElement("i", { "class": "fa fa-facebook-square fa-3x" })
                    ),
                    React.createElement(
                        "a",
                        { "class": "twitterlink", href: "https://www.twitter.com" },
                        React.createElement("i", { "class": "fa fa-twitter-square fa-3x" })
                    ),
                    React.createElement(
                        "a",
                        { "class": "youtubelink", href: "https://www.youtube.com" },
                        React.createElement("i", { "class": "fa fa-youtube-square fa-3x" })
                    ),
                    React.createElement(
                        "a",
                        { "class": "googlelink", href: "https://www.google.com" },
                        React.createElement("i", { "class": "fa fa-google-plus-square fa-3x" })
                    )
                )
            ),
            React.createElement(
                "div",
                { "class": "massofpostaside" },
                React.createElement(
                    "h2",
                    null,
                    "Recent posts"
                ),
                React.createElement(
                    "div",
                    { "class": "postaside" },
                    React.createElement(
                        "h2",
                        null,
                        React.createElement(
                            "a",
                            { href: "./POST01.html", title: "POST01", "class": "postlink postasidelink" },
                            "POST01"
                        )
                    ),
                    React.createElement(
                        "div",
                        { "class": "fadeaside" },
                        " "
                    ),
                    React.createElement(
                        "p",
                        { "class": "postingaside" },
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac orci elementum, commodo arcu ac, commodo metus. Aenean facilisis suscipit orci, vitae egestas enim ultricies suscipit. Quisque lacinia egestas dolor. Quisque vitae sollicitudin ipsum, sit amet aliquet libero. Ut sed finibus ipsum. Nam vestibulum id lacus et pharetra. Duis quis enim quam. In hac habitasse platea dictumst. Sed congue lectus quis erat sollicitudin lacinia. Aenean porttitor fermentum porttitor. Morbi sit amet metus volutpat, vehicula libero vel, cursus tortor. Mauris elementum risus urna, at semper diam bibendum vel. Cras volutpat tellus a magna rhoncus dictum. Nunc maximus turpis dictum dui commodo tempor. Nunc suscipit, purus in egestas tincidunt, est lorem bibendum ex, at pulvinar nunc orci nec lorem. Nullam nec massa iaculis, ullamcorper metus at, tempor augue. Integer accumsan blandit nisi et venenatis. Phasellus ac pharetra elit. Vestibulum quis nulla eget dolor congue tempus. Aliquam erat volutpat. Donec ultricies sem et nisl aliquet cursus. Sed sed risus tincidunt, lobortis magna in, blandit enim. Mauris mattis laoreet elit in accumsan. Vivamus efficitur nisi at ipsum facilisis convallis. Vestibulum quis tincidunt dui, eu feugiat sem. Fusce imperdiet massa metus, sit amet hendrerit diam tincidunt eu. Aliquam erat volutpat. Sed sit amet fermentum sapien, vel porttitor sem. Nunc ullamcorper convallis felis, a dictum ante consectetur eget. Ut sit amet pellentesque ante. Nulla at nisi imperdiet, ullamcorper dolor nec, posuere est. Curabitur semper ultricies scelerisque. Suspendisse auctor in diam quis iaculis. Integer mollis velit at turpis porttitor, sed gravida tortor tempus. Donec placerat lectus a turpis auctor faucibus in vitae odio. Sed enim sapien, blandit et sem eu, suscipit molestie velit. Nunc ultrices tortor vel elit porta, eu tincidunt lacus laoreet. Morbi gravida sed quam blandit ultrices. Pellentesque interdum lorem sit amet mattis commodo. Etiam et lectus euismod, mollis mi et, mattis lacus. Ut eget pulvinar magna, ornare ultrices mi. Curabitur hendrerit neque non sem sagittis ultricies. Suspendisse massa erat, hendrerit at turpis cursus, iaculis maximus massa. Mauris vel porta mi, quis ornare sem. Nam vel quam turpis. Nullam viverra ex augue, luctus dictum nunc tincidunt vel. Morbi facilisis elit a tempor scelerisque. Donec elementum posuere dui nec congue. Fusce viverra eros auctor facilisis facilisis. Nulla facilisi. Praesent sem justo, sodales mollis cursus quis, sollicitudin ut metus. Mauris risus nulla, auctor laoreet hendrerit nec, dignissim ut sem."
                    )
                ),
                React.createElement(
                    "div",
                    { "class": "postaside" },
                    React.createElement(
                        "h2",
                        null,
                        React.createElement(
                            "a",
                            { href: "#", title: "POST02", "class": "postlink postasidelink" },
                            "POST02"
                        )
                    ),
                    React.createElement(
                        "div",
                        { "class": "fadeaside" },
                        " "
                    ),
                    React.createElement(
                        "p",
                        { "class": "postingaside" },
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac orci elementum, commodo arcu ac, commodo metus. Aenean facilisis suscipit orci, vitae egestas enim ultricies suscipit. Quisque lacinia egestas dolor. Quisque vitae sollicitudin ipsum, sit amet aliquet libero. Ut sed finibus ipsum. Nam vestibulum id lacus et pharetra. Duis quis enim quam. In hac habitasse platea dictumst. Sed congue lectus quis erat sollicitudin lacinia. Aenean porttitor fermentum porttitor. Morbi sit amet metus volutpat, vehicula libero vel, cursus tortor. Mauris elementum risus urna, at semper diam bibendum vel. Cras volutpat tellus a magna rhoncus dictum. Nunc maximus turpis dictum dui commodo tempor. Nunc suscipit, purus in egestas tincidunt, est lorem bibendum ex, at pulvinar nunc orci nec lorem. Nullam nec massa iaculis, ullamcorper metus at, tempor augue. Integer accumsan blandit nisi et venenatis. Phasellus ac pharetra elit. Vestibulum quis nulla eget dolor congue tempus. Aliquam erat volutpat. Donec ultricies sem et nisl aliquet cursus. Sed sed risus tincidunt, lobortis magna in, blandit enim. Mauris mattis laoreet elit in accumsan. Vivamus efficitur nisi at ipsum facilisis convallis. Vestibulum quis tincidunt dui, eu feugiat sem. Fusce imperdiet massa metus, sit amet hendrerit diam tincidunt eu. Aliquam erat volutpat. Sed sit amet fermentum sapien, vel porttitor sem. Nunc ullamcorper convallis felis, a dictum ante consectetur eget. Ut sit amet pellentesque ante. Nulla at nisi imperdiet, ullamcorper dolor nec, posuere est. Curabitur semper ultricies scelerisque. Suspendisse auctor in diam quis iaculis. Integer mollis velit at turpis porttitor, sed gravida tortor tempus. Donec placerat lectus a turpis auctor faucibus in vitae odio. Sed enim sapien, blandit et sem eu, suscipit molestie velit. Nunc ultrices tortor vel elit porta, eu tincidunt lacus laoreet. Morbi gravida sed quam blandit ultrices. Pellentesque interdum lorem sit amet mattis commodo. Etiam et lectus euismod, mollis mi et, mattis lacus. Ut eget pulvinar magna, ornare ultrices mi. Curabitur hendrerit neque non sem sagittis ultricies. Suspendisse massa erat, hendrerit at turpis cursus, iaculis maximus massa. Mauris vel porta mi, quis ornare sem. Nam vel quam turpis. Nullam viverra ex augue, luctus dictum nunc tincidunt vel. Morbi facilisis elit a tempor scelerisque. Donec elementum posuere dui nec congue. Fusce viverra eros auctor facilisis facilisis. Nulla facilisi. Praesent sem justo, sodales mollis cursus quis, sollicitudin ut metus. Mauris risus nulla, auctor laoreet hendrerit nec, dignissim ut sem."
                    )
                ),
                React.createElement(
                    "div",
                    { "class": "postaside" },
                    React.createElement(
                        "h2",
                        null,
                        React.createElement(
                            "a",
                            { href: "#", title: "POST03", "class": "postlink postasidelink" },
                            "POST03"
                        )
                    ),
                    React.createElement(
                        "div",
                        { "class": "fadeaside" },
                        " "
                    ),
                    React.createElement(
                        "p",
                        { "class": "postingaside" },
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac orci elementum, commodo arcu ac, commodo metus. Aenean facilisis suscipit orci, vitae egestas enim ultricies suscipit. Quisque lacinia egestas dolor. Quisque vitae sollicitudin ipsum, sit amet aliquet libero. Ut sed finibus ipsum. Nam vestibulum id lacus et pharetra. Duis quis enim quam. In hac habitasse platea dictumst. Sed congue lectus quis erat sollicitudin lacinia. Aenean porttitor fermentum porttitor. Morbi sit amet metus volutpat, vehicula libero vel, cursus tortor. Mauris elementum risus urna, at semper diam bibendum vel. Cras volutpat tellus a magna rhoncus dictum. Nunc maximus turpis dictum dui commodo tempor. Nunc suscipit, purus in egestas tincidunt, est lorem bibendum ex, at pulvinar nunc orci nec lorem. Nullam nec massa iaculis, ullamcorper metus at, tempor augue. Integer accumsan blandit nisi et venenatis. Phasellus ac pharetra elit. Vestibulum quis nulla eget dolor congue tempus. Aliquam erat volutpat. Donec ultricies sem et nisl aliquet cursus. Sed sed risus tincidunt, lobortis magna in, blandit enim. Mauris mattis laoreet elit in accumsan. Vivamus efficitur nisi at ipsum facilisis convallis. Vestibulum quis tincidunt dui, eu feugiat sem. Fusce imperdiet massa metus, sit amet hendrerit diam tincidunt eu. Aliquam erat volutpat. Sed sit amet fermentum sapien, vel porttitor sem. Nunc ullamcorper convallis felis, a dictum ante consectetur eget. Ut sit amet pellentesque ante. Nulla at nisi imperdiet, ullamcorper dolor nec, posuere est. Curabitur semper ultricies scelerisque. Suspendisse auctor in diam quis iaculis. Integer mollis velit at turpis porttitor, sed gravida tortor tempus. Donec placerat lectus a turpis auctor faucibus in vitae odio. Sed enim sapien, blandit et sem eu, suscipit molestie velit. Nunc ultrices tortor vel elit porta, eu tincidunt lacus laoreet. Morbi gravida sed quam blandit ultrices. Pellentesque interdum lorem sit amet mattis commodo. Etiam et lectus euismod, mollis mi et, mattis lacus. Ut eget pulvinar magna, ornare ultrices mi. Curabitur hendrerit neque non sem sagittis ultricies. Suspendisse massa erat, hendrerit at turpis cursus, iaculis maximus massa. Mauris vel porta mi, quis ornare sem. Nam vel quam turpis. Nullam viverra ex augue, luctus dictum nunc tincidunt vel. Morbi facilisis elit a tempor scelerisque. Donec elementum posuere dui nec congue. Fusce viverra eros auctor facilisis facilisis. Nulla facilisi. Praesent sem justo, sodales mollis cursus quis, sollicitudin ut metus. Mauris risus nulla, auctor laoreet hendrerit nec, dignissim ut sem."
                    )
                )
            )
        ),
        React.createElement(
            "div",
            { "class": "nextpage" },
            React.createElement(
                "div",
                null,
                React.createElement(
                    "a",
                    { href: "./blog.html", id: "nextpage" },
                    React.createElement("i", { "class": "fa fa-angle-right fa-2x" })
                ),
                React.createElement("i", { "class": "fa fa-circle", "aria-hidden": "true" }),
                React.createElement(
                    "a",
                    { href: "./blog.html", id: "lastpage" },
                    React.createElement("i", { "class": "fa fa-angle-double-right fa-2x" })
                )
            )
        ),
        React.createElement(
            "footer",
            null,
            React.createElement(
                "h3",
                null,
                "Copyright 2017"
            )
        ),
        React.createElement("div", { "class": "elv" }),
        React.createElement("script", { src: "https://cdn.polyfill.io/v2/polyfill.min.js" }),
        React.createElement("script", { src: "./popup2.js" }),
        React.createElement("script", { src: "./myScript.js" }),
        React.createElement("script", { src: "./udvozlo.js" })
    )
);