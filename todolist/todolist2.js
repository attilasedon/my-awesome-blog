let allTasksString = localStorage.getItem("savedTasks");
localStorage.removeItem("savedTasks");
let allTasks = JSON.parse(allTasksString);
if(allTasks === null){
allTasks = [{ content: "This is the first task", done: true }];

window.fetch("todolist.json").then(
    request => request.json()
).then(
    tasks => {
        allTasks = tasks.concat(allTasks);
        displayTasksFromArray(allTasks);
    }
)
}

displayTasksFromArray(allTasks);




function displayTasksFromArray(taskArray) {
    document.querySelector("section > ul").innerHTML = "";
    displayTask(taskArray);
}

function createTask(tasks, todo) {
    let task = {
        content: todo,
        done: false
    }
    tasks.push(task);
    var tasksString = JSON.stringify(tasks);
    localStorage.setItem("savedTasks", tasksString);
    console.log(tasks);
    displayTask(tasks);
}

function displayTask(tasks) {
console.log("Display");
console.log(tasks);
/*var taskDone = tasks.slice().filter(object => {
    if(object.done===true){
        return object;
    }
});
var taskNotDone = tasks.slice().filter(object => {
    if(object.done===false){
        return object;
    }
});
tasks = taskNotDone.concat(taskDone);*/
tasks.sort(function (a,b){
    if(a.done===false && b.done===true){
        return -1;
    }
    if(a.done===true && b.done===false){
        return 1;
    }
    if(a.done === b.done){
        return 0;
    }
})
console.log(tasks);

document.querySelector("section > ul").innerHTML = "";

tasks.forEach(function(task){
    if (task !== undefined && task.content !== "") {
        /*
        let template = `<li>
    <span class="fa `; 
        if(task.done){
            template += "fa-check-square-o"
        }
        else {
            template += "fa-square-o"
        }
        template += ` sfa-2x"></span>
    <span>${task.content}</span>
    <span class="fa fa-times"></span>
</li>`;
*/
        /*
        let template = `<li>
        <span class="fa ${task.done ? "fa-check-square-o" : "fa-square-o"} fa-2x"></span>
        <span>${task.content}</span>
        <span class="fa fa-times"></span>
        </li>`;*/
        let template = `<li class= "${task.done ? "done": ""}">
                <span class="fa fa-square-o fa-2x"></span>
                <span>${task.content}</span>
                <span class="fa fa-times"></span>
            </li>`
        document.querySelector("section > ul").insertAdjacentHTML('beforeend', template);
        document.querySelector("section > ul > li:last-child > span:first-of-type").addEventListener("click", markDone);
        document.querySelector("section > ul > li:last-child > span:last-of-type").addEventListener("click", deleteTask);
        if(task.done){
            document.querySelector("section > ul > li:last-child > span:first-of-type").classList.add("fa-check-square-o");
            document.querySelector("section > ul > li:last-child > span:first-of-type").classList.remove("fa-square-o");
        }
    }
})
}
//Event handler for click on the square in any of the todo list
function markDone(event) {
    allTasks.forEach(item => {
        if (item.content === event.target.nextElementSibling.textContent){
            item.done = true;
        }
    })
    event.target.parentElement.classList.add("done");
    event.target.classList.remove("fa-square-o");
    event.target.classList.add("fa-check-square-o");
    event.target.removeEventListener("click", markDone);

}

//Deletes a task
function deleteTask(event){
    console.log("delete task happened");
    for(i=0; i < allTasks.length; i++){
        if(allTasks[i].content === event.target.previousElementSibling.textContent){
            allTasks = allTasks.slice(i,i+1);
    }
    event.target.parentElement.remove();
}
}


document.querySelector("form > button").addEventListener('click', function (event) {
    createTask(allTasks, event.target.previousElementSibling.value);
    event.target.previousElementSibling.value = "";
});

document.querySelector("form > input").addEventListener('keypress', function (event) {
    if (event.key == "Enter") {
        event.preventDefault();
        createTask(allTasks, event.target.value);
        event.target.value = "";
    }
});

/*document.querySelectorAll("section > ul > li > span.fa-square-o:first-of-type").forEach(item => {
    item.addEventListener("click", markDone);
});
document.querySelectorAll("section > ul > li > span:last-of-type").forEach(item => {
    item.addEventListener("click", deleteTask);
});*/

