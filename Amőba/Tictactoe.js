let cell = document.querySelectorAll('.cell');
let footerleft = document.querySelector("#footerleft");
let footerright = document.querySelector("#footerright");
let leftdiv = document.querySelector("#footerleft>h2");
let rightdiv = document.querySelector("#footerright>h2");

let nev1;
let nev2;

document.querySelector("#deleteButton").onClick = function(event){
    localStorage.removeItem("nev1mentett");
    localStorage.removeItem("nev2mentett");
    localStorage.removeItem("win");
    localStorage.removeItem("nev1Gyoz");
    localStorage.removeItem("nev2Gyoz"); 
}

let nev1mentett = localStorage.getItem("nev1mentett");
let nev2mentett = localStorage.getItem("nev2mentett");

let zold;
let piros;

let nev1Score=0;
let nev2Score=0;
let nameWinner;
let winner = localStorage.getItem("win");
let pont1 = localStorage.getItem("nev1Gyoz");
let pont2 = localStorage.getItem("nev2Gyoz");

//Checking if someone had already played, and who won.
if(winner !== null){
    if(winner !== "nincs"){
document.querySelector("header>div>h2").textContent = "Előző kör győztese:"+winner;
}else{
document.querySelector("header>div>h2").textContent = "Előző kör döntetlen eredménnyel zárult";
}
//Writing out the points to the left side, next to the name.
if(pont1===null){
    pont1 = 0;}
if(pont2===null){
    pont2 = 0;
}
leftdiv.textContent = nev2mentett + ": " + pont2;
rightdiv.textContent = nev1mentett + ": "+ pont1;

}else{

//If no one played, asks the name of the players.

nev1 = window.prompt("Mi a neved, Player One?");
rightdiv.textContent = nev1;
localStorage.setItem("nev1mentett", nev1);

nev2 = window.prompt("Mi a neved, Player Two?");
leftdiv.textContent = nev2;
localStorage.setItem("nev2mentett", nev2);
}

//Asks, who should start? Then gives values to the players, according to the question.

let b = window.confirm("Zöld kezdjen?")

if(b===true){
    zold=1;
    piros=0;
}
else{
    zold=0;
    piros=1;
}

//The function to rule them all!! Basically, you click magic happens.

cell.forEach(currentCell => {currentCell.onclick = function(event){
//Examines if the cells already filled.
        console.log=("Kattintva");
        if(currentCell.classList.contains("x")===true||currentCell.classList.contains("o")===true){
        }else{
//Fills the cell, changes the players.
        if (zold === 1 && piros === 0){
            currentCell.classList.add("o");
            currentCell.textContent = "o";
            piros = 1;
            zold = 0;
            footerleft.style.color="red";
            footerright.style.color="white";   
        } else if(zold === 0 && piros === 1) 
            {currentCell.classList.add("x");
            currentCell.textContent = "x";
            piros = 0;
            zold = 1;
            footerleft.style.color="white";
            footerright.style.color="green"; 
        }
//Checks, if someone wins, or it's a draw, after every click.  
function checkWin(){
        xarray=[];
        oarray=[];
        array=[];
        total = 0;
    
        for(i=0;i<9; i++){
            if(cell[i].classList.contains("x")===true) {
             xarray[i]=1;}
            if(cell[i].classList.contains("o")===true) {
             oarray[i]=1;}

            if((xarray[0]===1 && xarray[1]===1 && xarray[2]===1) ||
                (xarray[3]===1 && xarray[4]===1 && xarray[5]===1) ||
                (xarray[6]===1 && xarray[7]===1 && xarray[8]===1) ||
                (xarray[0]===1 && xarray[3]===1 && xarray[6]===1) || 
                (xarray[0]===1 && xarray[4]===1 && xarray[8]===1) || 
                (xarray[1]===1 && xarray[4]===1 && xarray[7]===1) || 
                (xarray[2]===1 && xarray[5]===1 && xarray[8]===1) || 
                (xarray[2]===1 && xarray[4]===1 && xarray[6]===1)){

                return "x";
            }

            if((oarray[0]===1 && oarray[1]===1 && oarray[2]===1) ||
            (oarray[3]===1 && oarray[4]===1 && oarray[5]===1) ||
            (oarray[6]===1 && oarray[7]===1 && oarray[8]===1) ||
            (oarray[0]===1 && oarray[3]===1 && oarray[6]===1) || 
            (oarray[0]===1 && oarray[4]===1 && oarray[8]===1) || 
            (oarray[1]===1 && oarray[4]===1 && oarray[7]===1) || 
            (oarray[2]===1 && oarray[5]===1 && oarray[8]===1) || 
            (oarray[2]===1 && oarray[4]===1 && oarray[6]===1)){

                return "o";
            }
            
            if(cell[i].classList.contains("x")===true || cell[i].classList.contains("o")===true){
                    total = total + 1;}
                }
                if(total === 9){
                    return "draw"
                }

            }
//Writes out, who won, if it's a draw, then it writes out "draw". Saves who won the game into the local storage. 
//There is a problem here. After the second match it writes out: undefined won!
        f= checkWin();
        if(f==="x"){
            nev2Score= nev2Score + 1;
            nameWinner = nev2 + " győzött"; 
            window.alert(nameWinner);
            localStorage.removeItem("win");
            localStorage.setItem("win", nev2);} 
        if(f==="o"){
            nev1Score=nev1Score + 1;
            nameWinner = nev1 + " győzött"; 
            window.alert(nameWinner);
            localStorage.removeItem("win");
            localStorage.setItem("win", nev1);
        }
        if(f==="draw"){
            window.alert("Döntetlen");
            localStorage.removeItem("win");
            localStorage.setItem("win", "nincs");
        }
//Saves the scores into the local storage.
        localStorage.setItem("nev1Gyoz", nev1Score);
        localStorage.setItem("nev2Gyoz", nev2Score);
    } 

}})
